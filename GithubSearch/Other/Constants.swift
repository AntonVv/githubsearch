//
//  Constants.swift
//  GithubSearch
//
//  Created by Mac on 16.09.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation

struct EndPoints {
    static let baseURL = "https://api.github.com/search/repositories"
    static let homeURL = "https://github.com"
}

struct DefaultsKeys {
    static let previousSearches = "PreviousSearches"
}

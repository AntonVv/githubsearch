    //
//  ShowPreviousSearches.swift
//  GithubSearch
//
//  Created by Mac on 16.09.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation

typealias ShowPreviousSearchesUseCaseCompletionHandler = (_ searches: Result<[String]>) -> Void

protocol ShowPreviousSearchesUseCase {
    func displaySearches(completionHandler: @escaping ShowPreviousSearchesUseCaseCompletionHandler)
}

final class ShowPreviousSearchesUseCaseImplementation: ShowPreviousSearchesUseCase {
    
    // MARK: - ShowPreviousSearchesUseCase
    
    func displaySearches(completionHandler: @escaping ShowPreviousSearchesUseCaseCompletionHandler) {

        if let saved =  UserDefaults.standard.object(forKey: DefaultsKeys.previousSearches) as? [String:TimeInterval] {
            
            var searches = saved.sorted{$0.value > $1.value}.map{ $0.key}
            
            completionHandler(.success(searches))
            
        } else {
            completionHandler(.success([]))
        }
    }
}

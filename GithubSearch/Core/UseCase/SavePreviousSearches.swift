//
//  SavePreviousSearches.swift
//  GithubSearch
//
//  Created by Mac on 18.09.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation

protocol SavePreviousSearchesUseCase {
    
    func saveSearch(_ search: String)
}

class SavePreviousSearchesUseCaseImplementation: SavePreviousSearchesUseCase {
    
    func saveSearch(_ search: String) {
        
        guard var saved = UserDefaults.standard.object(forKey: DefaultsKeys.previousSearches) as? [String:TimeInterval] else {
            UserDefaults.standard.set([search:NSDate().timeIntervalSince1970], forKey: DefaultsKeys.previousSearches)
            return
        }
        
        saved[search] = NSDate().timeIntervalSince1970
        UserDefaults.standard.set(saved, forKey: DefaultsKeys.previousSearches)
    }
}

//
//  SearchRepoApiRequest.swift
//  GithubSearch
//
//  Created by Mac on 16.09.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation


struct SearchRepoApiRequest: ApiRequest {
    
    let searchedRepoName: String
    
    var urlRequest: URLRequest {
        let url: URL! = URL(string: EndPoints.baseURL + "?q=\(self.searchedRepoName)&sort=stars&order=desc")
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        return request
    }
}


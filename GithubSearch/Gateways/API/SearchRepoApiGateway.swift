//
//  SearchRepoApiGateway.swift
//  GithubSearch
//
//  Created by Mac on 16.09.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation

protocol SearchRepoApiGateway {
    
    func searchRepo(for name:String, completionHandler:@escaping (_ weather: Result<[Repo]>) -> Void)
}

final class SearchRepoApiGatewayImplementation: SearchRepoApiGateway {
    
    let searchRepoService: SearchRepoApiService
    
    init(searchRepoService: SearchRepoApiService) {
        self.searchRepoService = searchRepoService
    }
    
    // MARK: - SearchRepoApiGateway
    
    
    func searchRepo(for name: String, completionHandler: @escaping (Result<[Repo]>) -> Void) {
        
        let searchRepoRequest = SearchRepoApiRequest(searchedRepoName: name)
        
        searchRepoService.execute(request: searchRepoRequest) { (result: Result<ApiResponse<[Repo]>>) in
            
            switch result {
            case let .success(response):
                let repo = response.entity
                completionHandler(.success(repo))
            case let .failure(error):
                completionHandler(.failure(error))
            }
        }
    }
}

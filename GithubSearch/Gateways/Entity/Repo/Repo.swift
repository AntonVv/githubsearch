//
//  Repo.swift
//  GithubSearch
//
//  Created by Mac on 16.09.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation

struct Repo: InitializableWithData, InitializableWithJson {
    let id: Int
    let name: String
    let fullName: String
    let url: URL
    var description: String
    let language: String
    let countOfStars: Int
    let score: Double
    
    init(data: Data?) throws {
        // Here you can parse the JSON or XML using the build in APIs or your favorite libraries
        guard let data = data,
            let jsonObject = try? JSONSerialization.jsonObject(with: data),
            let json = jsonObject as? [String: Any] else {
                throw NSError.createParseError()
        }
        
        try self.init(json: json)
        
    }
    
    init(json: [String : Any]) throws {
        
        guard let id = json["id"] as? Int,
            let name = json["name"] as? String,
            let fullName = json["full_name"] as? String,
            let urlString = json["html_url"] as? String,
            let countOfStars = json["stargazers_count"] as? Int,
            let score = json["score"] as? Double
            else {
                throw NSError.createParseError()
        }
        
        self.id = id
        self.name = name
        self.fullName = fullName
        self.url = URL(string: urlString) ?? URL(string:EndPoints.homeURL)!
        
        self.countOfStars = countOfStars
        self.score = score
        
        self.description = ""

        if let description = json["description"] as? String {
            self.description = description
        }

        guard let language = json["language"] as? String else {
            self.language = ""
            return
        }
        self.language = language

    }
}

extension Array: InitializableWithData {
    
    init(data: Data?) throws {
        guard let data = data,
            let jsonObject = try? JSONSerialization.jsonObject(with: data),
            let json = jsonObject as? Dictionary<String,Any>,
            let jsonArray = json["items"] as? [Dictionary<String,Any>] else {
                throw NSError.createParseError()
        }
        
        guard let element = Element.self as? InitializableWithJson.Type else {
            throw NSError.createParseError()
        }
        
        self = try jsonArray.map( { return try element.init(json: $0) as! Element } )
    }
}

extension Repo: Equatable { }

func == (lhs: Repo, rhs: Repo) -> Bool {
    return lhs.id == rhs.id
}

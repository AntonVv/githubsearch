//
//  SearchRepoRouter.swift
//  GithubSearch
//
//  Created by Mac on 16.09.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import UIKit

protocol SearchRepoRouter {
    func prepare(for segue: UIStoryboardSegue, sender: Any?)
    func presentRepo(_ repo: Repo)
}

class SearchRepoRouterImplementation: SearchRepoRouter {
    
    fileprivate weak var searchRepoViewController: SearchRepoViewController?

    init(searchRepoViewController:SearchRepoViewController ) {
        self.searchRepoViewController = searchRepoViewController
    }
    func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
    }
    
    func presentRepo(_ repo: Repo) {
        
        let repoController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RepoViewController")
        
        searchRepoViewController?.navigationController?.pushViewController(repoController, animated: true)
        
        repoController.navigationItem.title = repo.name
        
        let webView = UIWebView(frame: repoController.view.frame)

        repoController.view.addSubview(webView)
        webView.loadRequest(URLRequest(url: repo.url))
    }
}

//
//  SearchRepoViewController.swift
//  GithubSearch
//
//  Created by Mac on 16.09.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import UIKit

class SearchRepoViewController: UIViewController, SearchRepoView {

    var configurator = SearchRepoConfiguratorImplementation()
    var presenter: ReposPresenter!
    var indicator: UIActivityIndicatorView = {
       return UIActivityIndicatorView(activityIndicatorStyle:UIActivityIndicatorViewStyle.gray)
    }()

    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet fileprivate weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configurator.configure(searchRepoViewController: self)
        presenter.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.tableView.estimatedRowHeight = 109
        self.tableView.keyboardDismissMode = .interactive
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidAppear(_:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - SearchRepoView
    func refreshReposView() {
        self.tableView.reloadData()
    }
    
    func endEditing() {
        self.view.endEditing(true)
    }
    
    func activityIndicatorIsShown(_ show: Bool) {
        
        if show {
            indicator.startAnimating()
            indicator.center = self.tableView.center
            self.tableView.addSubview(indicator)
        } else {
            indicator.removeFromSuperview()
            indicator.stopAnimating()
        }
    }
    
    func willSearchText(_ text: String) {
        self.searchBar.text = text
    }
    
}


extension SearchRepoViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.presenter.numberOfRows
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return self.presenter.cell(tableView: tableView, forRow: indexPath)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        self.presenter.didSelect(row: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

extension SearchRepoViewController: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {

        self.presenter.searchWithText(text: searchBar.text)
    }
    
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        self.presenter.beginSearching()
        return true
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.endEditing(true)
        self.presenter.cancelSearching()
    }
}

extension SearchRepoViewController {
    func keyboardDidAppear(_ notification: Notification) {
        if let rect = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            var inset = self.tableView.contentInset
            inset.bottom = rect.cgRectValue.size.height
            self.tableView.contentInset = inset
        }
    }
    
    func keyboardWillDisappear(_ notification: Notification) {
        var inset = self.tableView.contentInset
        inset.bottom = 0
        self.tableView.contentInset = inset
    }
}

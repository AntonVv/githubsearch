//
//  SearchRepoConfigurator.swift
//  GithubSearch
//
//  Created by Mac on 16.09.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import Foundation

protocol SearchRepoConfigurator {
    func configure(searchRepoViewController: SearchRepoViewController)
}


class SearchRepoConfiguratorImplementation:SearchRepoConfigurator   {
    
    func configure(searchRepoViewController: SearchRepoViewController) {
        
        let router = SearchRepoRouterImplementation(searchRepoViewController: searchRepoViewController)
        let previousSearchesUseCase = ShowPreviousSearchesUseCaseImplementation()
        let saveSearchesUseCase = SavePreviousSearchesUseCaseImplementation()

        
        let searchRepoApiService = SearchRepoApiServiceImplementation(urlSessionConfiguration: URLSessionConfiguration.default, completionHandlerQueue: OperationQueue.main)
        
        let searchRepoGateway = SearchRepoApiGatewayImplementation(searchRepoService: searchRepoApiService)
        
        let presenter = ReposPresenterImplementation(view: searchRepoViewController, router: router, previousSearchesUseCase: previousSearchesUseCase,saveSearchesUseCase: saveSearchesUseCase,
                                                     searchRepoGateway: searchRepoGateway)
        searchRepoViewController.presenter = presenter
        
    }
}

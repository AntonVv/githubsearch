//
//  RepoTableViewCell.swift
//  GithubSearch
//
//  Created by Mac on 16.09.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import UIKit

class RepoTableViewCell: UITableViewCell, RepoCellView {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var languageLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    // MARK: - RepoCellView
    
    func display(repo: Repo?) {
        if let repo = repo {
            self.nameLabel.text = repo.name
            self.descriptionLabel.text = repo.description
            self.languageLabel.text = repo.language
            self.starsLabel.text = "\(repo.countOfStars)★"
        }
    }
    
    func display(name: String) {
        self.textLabel?.text = name
    }

}

extension RepoTableViewCell {
    static func cellReuseIdentifier() -> String {
        return String(describing: self)
    }
    
    static func cellPreviousSearchReuseIdentifier() -> String {
        return "Cell"
    }
}



//
//  SearchRepoPresenter.swift
//  GithubSearch
//
//  Created by Mac on 16.09.17.
//  Copyright © 2017 Volobuiev Anton. All rights reserved.
//

import UIKit

enum SearchState {
    case inactive
    case active
}

protocol SearchRepoView: class {
    func refreshReposView()
    func endEditing()
    func activityIndicatorIsShown(_ show: Bool)
    func willSearchText(_ text: String)
}

protocol RepoCellView {
    func display(repo: Repo?)
    func display(name: String)
}

protocol ReposPresenter {
    var numberOfRows: Int {get}    
    func viewDidLoad()
    func cell(tableView: UITableView, forRow indexPath: IndexPath) -> UITableViewCell
    func didSelect(row: Int)
    func searchWithText(text:String?)
    func beginSearching()
    func cancelSearching()
}

final class ReposPresenterImplementation: ReposPresenter {

    fileprivate weak var view: SearchRepoView?
    internal let router: SearchRepoRouter
    fileprivate let previousSearchesUseCase: ShowPreviousSearchesUseCase
    fileprivate let saveSearchesUseCase: SavePreviousSearchesUseCase

    fileprivate let searchRepoGateway: SearchRepoApiGateway
    var activeState = SearchState.inactive {
        didSet {
            if activeState == .inactive && self.previousSearches.count == 0 {
                activeState = .active
            }
            self.view?.refreshReposView()
        }
    }
    
    var repos = [Repo]()
    var previousSearches = [String]()
    
    init(view: SearchRepoView, router: SearchRepoRouter, previousSearchesUseCase: ShowPreviousSearchesUseCase, saveSearchesUseCase: SavePreviousSearchesUseCase, searchRepoGateway:SearchRepoApiGateway) {
        self.view = view
        self.router = router
        self.previousSearchesUseCase = previousSearchesUseCase
        self.saveSearchesUseCase = saveSearchesUseCase
        self.searchRepoGateway = searchRepoGateway
    }
    
    
    // MARK : - ReposPresenter
    var numberOfRows: Int {
        switch activeState {
        case .inactive:
            return previousSearches.count

        case .active:
            return repos.count
        }
    }
    
    
    
    func viewDidLoad() {
        displayPreviousSearches()
    }
    
    func displayPreviousSearches() {
        previousSearchesUseCase.displaySearches { (result) in
            switch result {
            case let .success(searches):
                self.handlePreviousSearches(searches)
            case let .failure(error):
                self.handlePreviousError(error)
            }
        }
    }
        
    func cell(tableView: UITableView, forRow indexPath: IndexPath) -> UITableViewCell {
        
        switch activeState {
            
            case .inactive:
                let cell = tableView.dequeueReusableCell(withIdentifier: RepoTableViewCell.cellPreviousSearchReuseIdentifier(), for: indexPath) as! RepoTableViewCell
                cell.display(name: self.previousSearches[indexPath.row])
                return cell
            
            case .active:
                let cell = tableView.dequeueReusableCell(withIdentifier: RepoTableViewCell.cellReuseIdentifier(), for: indexPath) as! RepoTableViewCell
                cell.display(repo: self.repos[indexPath.row])
                return cell

        }
    }
    
    func searchWithText(text: String?) {
        self.view?.endEditing()
        self.view?.activityIndicatorIsShown(true)
        self.activeState = .active
        if let text = text {
            self.searchRepoGateway.searchRepo(for: text) {[weak self] (result) in
                
                self?.saveSearchesUseCase.saveSearch(text)
                self?.view?.activityIndicatorIsShown(false)
                switch result {
                case let .success(searches):
                    self?.handleRepoSearches(searches)
                case let .failure(error):
                    self?.handleRepoSearchesError(error)
                }
            }
        } else {
            cancelSearching()
        }
        
    }
    
    func beginSearching() {
        self.activeState = .inactive
        displayPreviousSearches()
    }
    
    func cancelSearching() {
        self.activeState = .active
        displayPreviousSearches()
    }
    
    func didSelect(row: Int) {
        
        switch activeState {
        case .inactive:
            let text = self.previousSearches[row]
            self.view?.willSearchText(text)
            self.searchWithText(text: text)
            break
            
        case .active:
            router.presentRepo(self.repos[row])
            break
        }
        
    }
    
    
    // MARK: - Private
    
    func handlePreviousSearches(_ searches: [String]) {
        self.previousSearches = searches
        self.view?.refreshReposView()
    }
    
    func handlePreviousError(_ error: Error) {
        
    }
    
    func handleRepoSearches(_ searches: [Repo]) {
        self.repos = searches
        self.view?.refreshReposView()
    }
    
    func handleRepoSearchesError(_ error: Error) {
        
    }
    
}
